#ifndef MYCROFT_KIO_H_
#define MYCROFT_KIO_H_

#include <kio/global.h>
#include <kio/slavebase.h>

class MycroftProtocol : public QObject, public KIO::SlaveBase
{
    Q_OBJECT
public:
    MycroftProtocol(const QByteArray &pool, const QByteArray &app);
    void get(const QUrl &url) Q_DECL_OVERRIDE;
    
};

#endif