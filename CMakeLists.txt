add_definitions(-DTRANSLATION_DOMAIN=\"kio5_mycroft\")

cmake_minimum_required(VERSION 3.5)
set(QT_MIN_VERSION "5.5.0")

find_package(Qt5 ${QT_MIN_VERSION} REQUIRED CONFIG COMPONENTS Core DBus Network)
find_package(ECM ${KF5_MIN_VERSION} REQUIRED COMPONENTS I18n)
set(
    CMAKE_MODULE_PATH
        ${CMAKE_MODULE_PATH}
        ${ECM_MODULE_PATH}
        ${ECM_KDE_MODULE_DIR}
)

include(KDEInstallDirs)
include(KDECMakeSettings)
include(KDECompilerSettings NO_POLICY_SCOPE)
include(ECMSetupVersion)
include(FeatureSummary)
add_library(kio_mycroft MODULE kio_mycroft.cpp)
find_package(KF5 ${KF5_MIN_VERSION} REQUIRED KIO)
target_link_libraries(kio_mycroft KF5::KIOCore Qt5::Network Qt5::DBus)
set_target_properties(kio_mycroft PROPERTIES OUTPUT_NAME "mycroft")
install(TARGETS kio_mycroft DESTINATION ${KDE_INSTALL_PLUGINDIR}/kf5/kio)

########### install files ###############
install( FILES mycroft.protocol DESTINATION ${SERVICES_INSTALL_DIR} )