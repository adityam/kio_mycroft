## Mycroft Protocol KIO Slave
Simple slave to forward string intents to Mycroft

#### Installation:
- mkdir build  
- cd build  
- cmake -DCMAKE_INSTALL_PREFIX=/usr -DKDE_INSTALL_USE_QT_SYS_PATHS=TRUE ..  
- make  
- sudo make install  
- kdeinit5

#### Usage:
- "'kioclient5' 'exec' 'mycroft:/tell me a joke'"

#### Requires:
- Plasma-Mycroft
