#include "kio_mycroft.h"
#include <QCoreApplication>
#include <QtDBus>
#include <QDebug>

static QString sendQuery;

class KIOPluginForMetaData : public QObject
{
    Q_PLUGIN_METADATA(IID "org.kde.kio.slave.mycroft" FILE "mycroft.json")
};

extern "C"
{
    int Q_DECL_EXPORT kdemain(int argc, char **argv)
    {
        qDebug() << "Launching KIO slave.";
        if (argc != 4) {
            fprintf(stderr, "Usage: kio_hello protocol domain-socket1 domain-socket2\n");
            exit(-1);
        }
        MycroftProtocol slave(argv[2], argv[3]);
        slave.dispatchLoop();
        return 0;
    }
}

void MycroftProtocol::get(const QUrl &url)
{
    QString path = url.adjusted(QUrl::RemoveScheme).path();
    path = path.remove(0, 1);
    sendQuery = path;
    qDebug() << sendQuery;
    
    if(!QDBusConnection::sessionBus().isConnected())
    {
    fprintf(stderr, "Cannot connect to the D-Bus session bus.\n");
    finished();
    }
    QDBusInterface iface("org.kde.mycroftapplet", "/mycroftapplet", "", QDBusConnection::sessionBus());
    
    if(!iface.isValid())
  {
    fprintf(stderr, "Cannot connect to session bus.\n");
    finished();
  }
    else
    {
     mimeType("text/plain");   
     QByteArray str("Sent!\n");   
     data(str);
     iface.call("sendKioMethod", sendQuery);
     finished();
    }
}

MycroftProtocol::MycroftProtocol(const QByteArray &pool, const QByteArray &app)
    : SlaveBase("mycroft", pool, app) {}

#include "kio_mycroft.moc"

